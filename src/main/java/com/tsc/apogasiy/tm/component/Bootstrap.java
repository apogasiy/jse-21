package main.java.com.tsc.apogasiy.tm.component;

import main.java.com.tsc.apogasiy.tm.api.repository.*;
import main.java.com.tsc.apogasiy.tm.api.service.*;
import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import main.java.com.tsc.apogasiy.tm.command.auth.LoginCommand;
import main.java.com.tsc.apogasiy.tm.command.auth.LogoffCommand;
import main.java.com.tsc.apogasiy.tm.command.project.*;
import main.java.com.tsc.apogasiy.tm.command.system.*;
import main.java.com.tsc.apogasiy.tm.command.task.*;
import main.java.com.tsc.apogasiy.tm.command.user.*;
import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.exception.system.UnknownCommandException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.model.Task;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.repository.CommandRepository;
import main.java.com.tsc.apogasiy.tm.repository.ProjectRepository;
import main.java.com.tsc.apogasiy.tm.repository.TaskRepository;
import main.java.com.tsc.apogasiy.tm.repository.UserRepository;
import main.java.com.tsc.apogasiy.tm.service.*;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

import java.util.Optional;

public class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    private final ILogService logService = new LogService();
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthRepository authRepository = new AuthRepository();
    private final IAuthService authService = new AuthService(authRepository, userService);

    {
        registry(new DisplayAllCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());

        registry(new ProjectListShowCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskAddToProjectByIdProjectTaskCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListByProjectIdProjectTaskCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskRemoveFromProjectByIdProjectTaskCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new LoginCommand());
        registry(new LogoffCommand());

        registry(new UserListCommand());
        registry(new UserCreateCommand());
        registry(new UserClearCommand());
        registry(new UserChangeRoleCommand());
        registry(new UserDisplayByIdCommand());
        registry(new UserDisplayByLoginCommand());
        registry(new UserPasswordChangeCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUpdateByIdCommand());
        registry(new UserUpdateByLoginCommand());
    }

    public void start(final String... args) {
        displayWelcome();
        runArgs(args);
        initTestData();
        logService.debug("Test environment");
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }


    private void initTestData() {
        createDefaultUser();
        createDefaultData();
    }

    private void createDefaultData() {
        final String defaultUserId = userService.findByLogin("user").getId();
        final String defaultAdminId = userService.findByLogin("admin").getId();
        projectService.add(new Project(defaultAdminId, "Project 1", "-"));
        projectService.add(new Project(defaultAdminId, "Project 2", "-"));
        projectService.add(new Project(defaultUserId, "Project 3", "-"));
        projectService.add(new Project(defaultUserId, "Project 4", "-"));
        taskService.add(new Task(defaultAdminId, "Task 1", "Default Task"));
        taskService.add(new Task(defaultAdminId, "Task 2", "Default Task"));
        taskService.add(new Task(defaultUserId, "Task 3", "Default Task"));
        taskService.add(new Task(defaultUserId, "Task 4", "Default Task"));
        /*projectService.findByName(defaultAdminId, "Project 1").setUserId(defaultAdminId);
        projectService.findByName(defaultAdminId, "Project 2").setUserId(defaultAdminId);
        projectService.findByName(defaultUserId, "Project 3").setUserId(defaultUserId);
        projectService.findByName(defaultUserId, "Project 4").setUserId(defaultUserId);*/
        taskService.findByName(defaultAdminId, "Task 1").setProjectId(projectService.findByName(defaultAdminId, "Project 1").getId());
        taskService.findByName(defaultAdminId, "Task 2").setProjectId(projectService.findByName(defaultAdminId, "Project 2").getId());
        taskService.findByName(defaultUserId, "Task 3").setProjectId(projectService.findByName(defaultUserId, "Project 3").getId());
        taskService.findByName(defaultUserId, "Task 4").setProjectId(projectService.findByName(defaultUserId, "Project 4").getId());
    }

    private void createDefaultUser() {
        final User user = new User("user", "user");
        user.setRole(Role.USER);
        user.setEmail("user@folder.com");
        userService.add(user);
        final User admin = new User ("admin", "admin");
        admin.setRole(Role.ADMIN);
        user.setEmail("admin@folder.com");
        userService.add(admin);
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private boolean runArgs(final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0)
            return false;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (!Optional.ofNullable(command).isPresent())
            throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    private void runCommand(final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty())
            return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent())
            throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
