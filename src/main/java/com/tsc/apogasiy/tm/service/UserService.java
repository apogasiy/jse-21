package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IUserRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IUserService;
import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.exception.empty.*;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserEmailExistsException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserLoginExistsException;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(String login, String password) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        if (isLoginExists(login))
            throw new UserLoginExistsException(login);
        final User user = new User(login, HashUtil.encrypt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(String login, String password, String email) {
        User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(String userId, String password) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setPassword(HashUtil.encrypt(password));
        return user;
    }

    @Override
    public User setRole(String userId, Role role) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyLoginException();
        if (role == null)
            throw new EmptyRoleException();
        final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return user;

    }

    @Override
    public void remove(User user) {
        userRepository.remove(user);
    }

    @Override
    public User findByLogin(String login) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        if (!Optional.ofNullable(email).isPresent() || email.isEmpty())
            throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User removeByLogin(String login) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(String login) {
        return userRepository.findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public User updateById(String id, String lastName, String firstName, String middleName, String email) {
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        if (!Optional.ofNullable(lastName).isPresent() || lastName.isEmpty())
            throw new EmptyLastNameException();
        if (!Optional.ofNullable(firstName).isPresent() || firstName.isEmpty())
            throw new EmptyFirstNameException();
        if (!Optional.ofNullable(middleName).isPresent() || middleName.isEmpty())
            throw new EmptyMiddleNameException();
        if (!Optional.ofNullable(email).isPresent() || email.isEmpty())
            throw new EmptyEmailException();
        if (isEmailExists(email))
            throw new UserEmailExistsException(email);
        final User user = findById(id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateByLogin(String login, String lastName, String firstName, String middleName, String email) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty()) throw new EmptyIdException();
        if (isLoginExists(email))
            throw new UserLoginExistsException(login);
        if (!Optional.ofNullable(lastName).isPresent() || lastName.isEmpty())
            throw new EmptyLastNameException();
        if (!Optional.ofNullable(firstName).isPresent() || firstName.isEmpty())
            throw new EmptyFirstNameException();
        if (!Optional.ofNullable(middleName).isPresent() || middleName.isEmpty())
            throw new EmptyMiddleNameException();
        if (!Optional.ofNullable(email).isPresent() || email.isEmpty())
            throw new EmptyEmailException();
        final User user = findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

}
