package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IProjectTaskService;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;
    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String userId, final String projectId) {
        if (!Optional.ofNullable(projectId).isPresent() || projectId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskById(final String userId, final String projectId, final String taskId) {
        if (!Optional.ofNullable(projectId).isPresent() || projectId.isEmpty()) return null;
        if (!Optional.ofNullable(taskId).isPresent() || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String userId, final String projectId, final String taskId) {
        if (!Optional.ofNullable(projectId).isPresent() || projectId.isEmpty()) return null;
        if (!Optional.ofNullable(taskId).isPresent() || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    public Project removeById(final String userId, final String projectId) {
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        Project project = projectRepository.findByIndex(index);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        Project project = projectRepository.findByName(userId, name);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

}
