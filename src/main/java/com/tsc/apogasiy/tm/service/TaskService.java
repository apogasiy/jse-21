package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.api.service.ITaskService;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.exception.empty.*;
import main.java.com.tsc.apogasiy.tm.exception.entity.EmptyUserIdException;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        final Task task = new Task(userId, name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        if (description == null || description.isEmpty())
            throw new EmptyDescriptionException();
        final Task task = new Task(userId, name, description);
        taskRepository.add(task);
    }

    @Override
    public Task findByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task removeByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @Override
    public Task removeByIndex(String userId, Integer index) {
        return null;
    }

    @Override
    public Task updateById(final String userId, String id, String name, String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        final Task task = taskRepository.findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, Integer index, String name, String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public boolean existsByIndex(String userId, Integer index) {
        return false;
    }

    public boolean existsById(final String userId, String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return taskRepository.existsById(userId, id);
    }

    public boolean existsByIndex(final String userId, int index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return taskRepository.existsByIndex(userId, index);
    }

    @Override
    public boolean existsByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return taskRepository.existsByName(userId, name);
    }

    @Override
    public Task startById(final String userId, String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyNameException();
        return taskRepository.startById(userId, id);
    }

    @Override
    public Task startByIndex(final String userId, Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return taskRepository.startByIndex(userId, index);
    }

    @Override
    public Task startByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.startByName(userId, name);
    }

    @Override
    public Task finishById(final String userId, String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return taskRepository.finishById(userId, id);
    }

    @Override
    public Task finishByIndex(final String userId, Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return taskRepository.finishByIndex(userId, index);
    }

    @Override
    public Task finishByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.finishByName(userId, name);
    }

    @Override
    public Task changeStatusById(final String userId, String id, Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return taskRepository.changeStatusById(userId, id, status);
    }

    @Override
    public Task changeStatusByIndex(final String userId, Integer index, Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return taskRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    public Task changeStatusByName(final String userId, String name, Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return taskRepository.changeStatusByName(userId, name, status);
    }

}
