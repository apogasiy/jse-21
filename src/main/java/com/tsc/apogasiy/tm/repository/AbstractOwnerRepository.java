package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.IOwnerRepository;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyIdException;
import main.java.com.tsc.apogasiy.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public E add(String userId, E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        return list.stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String userId, final String id) {
        return findAll(userId).stream()
                .filter(entity -> entity.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public void clear(final String userId) {
        List<String> entityUserId = list.stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        entityUserId.forEach(list::remove);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (!Optional.ofNullable(entity).isPresent()) return null;
        remove(userId, entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (!Optional.ofNullable(entity).isPresent()) return null;
        remove(userId, entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> list = findAll(userId);
        list.remove(entity);
    }

    public Integer getSize(final String userId) {
        final List<E> list = findAll(userId);
        return list.size();
    }

}
