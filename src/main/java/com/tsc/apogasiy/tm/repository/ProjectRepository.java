package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Project;

import java.util.Optional;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public boolean existsByName(String userId, String name) {
        return findByName(userId, name) != null;
    }

    @Override
    public Project findByName(final String userId, final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Project removeByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        list.remove(project);
        return project;

    }

    @Override
    public Project startById(String userId, String id) {
        final Project project = findById(userId, id);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(String userId, Integer index) {
        final Project project = findByIndex(userId, index);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(String userId, String id) {
        final Project project = findById(userId, id);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(String userId, Integer index) {
        final Project project = findByIndex(userId, index);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(String userId, String id, Status status) {
        final Project project = findById(userId, id);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(String userId, Integer index, Status status) {
        final Project project = findByIndex(userId, index);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String userId, String name, Status status) {
        final Project project = findByName(userId, name);
        if (!Optional.ofNullable(project).isPresent())
            return null;
        project.setStatus(status);
        return project;
    }
    
}
