package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.IUserRepository;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.User;

import java.util.Locale;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        return list.stream()
                .filter(usr -> usr.getLogin().toLowerCase().equals(login.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(String email) {
        return list.stream()
                .filter(usr -> usr.getEmail().toLowerCase().equals(email.toLowerCase()))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(String email) {
        return findByEmail(email) != null;
    }

}
