package main.java.com.tsc.apogasiy.tm.api.repository;

public class AuthRepository implements IAuthRepository{

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
