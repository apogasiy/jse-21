package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import main.java.com.tsc.apogasiy.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void add(AbstractCommand command);


}
