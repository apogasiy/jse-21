package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IRepository;
import main.java.com.tsc.apogasiy.tm.model.AbstractEntity;

public interface IService <E extends AbstractEntity> extends IRepository<E> {

}
