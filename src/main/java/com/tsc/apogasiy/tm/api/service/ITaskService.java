package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Task;

public interface ITaskService extends IOwnerService<Task> {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Task findByName(String userId, String name);

    Task findByIndex(String userId, Integer index);

    Task removeByName(String userId, String name);

    Task removeByIndex(String userId, Integer index);

    Task updateById(String userId, final String id, final String name, final String description);

    Task updateByIndex(String userId, final Integer index, final String name, final String description);

    boolean existsByIndex(String userId, Integer index);

    boolean existsByName(String userId, String name);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

}
