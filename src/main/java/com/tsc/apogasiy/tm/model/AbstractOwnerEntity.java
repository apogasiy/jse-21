package main.java.com.tsc.apogasiy.tm.model;

public class AbstractOwnerEntity extends AbstractEntity {

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
